Examples
================================================================================

This chapter shows several examples of `histosaur` tool usage. The examples
maily work with timestamped data and show how it can be aggregated.
tool.

Messing with `/var/log/syslog`
--------------------------------------------------------------------------------

The `/var/log/syslog` file is an example of data file we might want to analyze
using `histosaur`. It consist of log lines and format may be different on each
system. In following example we will assume format like this:

```txt
Dec  9 17:20:42 host systemd[1]: logrotate.service: Succeeded.
Dec  9 17:20:42 host systemd[1]: Finished Rotate log files.
Dec  9 17:20:42 host kernel: [1859602.835760] snd_hdac_bus_update_rirb: 513 callbacks suppressed
Dec  9 17:20:42 host kernel: [1859602.835764] snd_hda_intel 0000:01:00.1: spurious response 0x0:0x0
```

**Note** The `histosaur` tool does not really 'count the lines'. We use the
fact that timestamp is present on all lines and count number of **same**
timestamps we find on lines that match regular expression.

#### Sort lines to bucket by time

We can count number of log messages per day and hour using simple regular expression:

```sh
$ histosaur -r '^(?P<date>...\s*\d\d? \d\d):\d\d' /var/log/syslog
>> Histogram: 'date' (buckets=3, marks=895)

  Dec  9 17   327 times ( 36.54%) ###################################
  Dec  9 18   458 times ( 51.17%) ##################################################
  Dec  9 19   110 times ( 12.29%) ############
```

We don't need to parse data from capture group to adjust bucket granularity
when working with dates. All we need is to adjust capture group. Notice that
regex is unchanged except position of capture group closing parenthesis:

```sh
$ histosaur -r '^(?P<date>...\s*\d\d? \d\d):\d\d' /var/log/syslog
>> Histogram: 'date' (buckets=60, marks=895)

  Dec  9 17:20    50 times (  5.59%) ##################################################
  Dec  9 17:22     5 times (  0.56%) #####
  Dec  9 17:25    34 times (  3.80%) ##################################
  ... snip lines ...
  Dec  9 19:09     8 times (  0.89%) ########
  Dec  9 19:10    28 times (  3.13%) ############################
  Dec  9 19:12     5 times (  0.56%) #####
```

#### Sort event occurences by time

We can also extend the regular expression to match only some lines to create
histogram of even occurences per time. For example, how many DHCP requests
there are in single hour:

```sh
$ histosaur -r '^(?P<date>...\s*\d\d? \d\d).*DHCPREQUEST' syslog
>> Histogram: 'date' (buckets=3, marks=26)

  Dec  9 17     9 times ( 34.62%) ################################
  Dec  9 18    14 times ( 53.85%) ##################################################
  Dec  9 19     3 times ( 11.54%) ##########
```


Messing with `git` history
--------------------------------------------------------------------------------

Using `histogram` tool for analyzing number of commits per time bucket or
number of commits by given person looks complicated, but is really simple.

We won't analyze git repository directly, but first dump data we are interested
in into separate file. For many uses we can use `git log` command:

```sh
$ git log --pretty="format:%as %an"
2020-11-24 John Doe
2020-10-16 John Doe
... snip lines ...
2020-12-09 John Doe
2020-11-24 John Doe
```

We can use `git log` to limit range of the commits (dates) we analyze using
appropriate arguments. That way we can analyze time period or commits between
releases.

**Note:** See `man git-log` for documentation

Once we have the data, we can use `histosaur` tool to run different analytics:

#### Number of commits per time bucket

The `histosaur` tool takes argument `-` to indicate that input should be read
from `stdin`. It can be used for on-the-fly filtering:

```sh
$ git log --pretty="format:%as %an" | histosaur -r '^(?P<date>\d{4}-\d{2})-\d{2}' - 
>> Histogram: 'date' (buckets=5, marks=37)

  2020-08     7 times ( 18.92%) ##########################
  2020-09    13 times ( 35.14%) ##################################################
  2020-10     6 times ( 16.22%) #######################
  2020-11     7 times ( 18.92%) ##########################
  2020-12     4 times ( 10.81%) ###############
```
