# Specification

The `histosaur` tool works with several data types - `field`, `value`,
`histogram`, etc. This section describes what each type is used for, if and how
it can be described on cli.

- [Regex and Field](./spec/regex_and_field.md)
- [Value](./spec/value.md)
- [Bucket](./spec/bucket.md)
- [Histogram](./spec/histogram.md)
