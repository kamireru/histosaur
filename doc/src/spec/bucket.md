Bucket Specification Format
================================================================================

This section provides overview of different bucket types used internally in
`histosaur` for data capture and way they can be specified on in [histogram][h]
configuration.

[h]:  ./histogram.md
[bu]: ../trouble.md#bucket-explosion

--------------------------------------------------------------------------------

Bucket Types
--------------------------------------------------------------------------------

The `histosaur` provides support for following bucket types:

| format        | name         | value types |
| -             | -            | -           |
| `<value>`     | value bucket | **all**     |
| `<min>-<max>` | range bucket | **all**     |
| `<v1/v2/v3>`  | list bucket  | **all**     |

The `value bucket` is mainly use with dynamic bucket generation strategy and is
suitable mostly for handling data that have small(ish) number of distinct values
like [`string`](./value#basic-types) or [calendar type](./value#calendar-types)
values.

It is **not** suitable for data that have big (infinite) range of distinct
values as its use would end with [bucket explosion][bu].

The `range bucket` is best suited for data with infinite number of distinct
values like [`int`, `float`](./value#basic-types) or
[date/time](./value#datetime-types)

> **Important:** The `range bucket` defines interval that is left-closed,
> right-open. Therefore integer sample `200` will match bucket `200-300`, but
> will **not** match bucket `100-200`.
>
> This behaviour is consistent with Rust ranges and allows sorting values like
> `float` which can fit in-between buckets just by adding another decimal point.

*TODO:* Add specification of custom labels for bucket

--------------------------------------------------------------------------------

Static vs Dynamic Bucket Creation
--------------------------------------------------------------------------------

The use we can get from a single bucket is not much. Most [histograms][h] will
be be sorting extracted data into multiple buckets to give us the overview we
need.

There are two ways we can go about bucket list creation:

- `static` where all `buckets` are created at startup form configuration
- `dynamic` where `buckets` are created from extracted values

The `static` bucket creation strategy is best suited processing input where we
already know what data we will extract and have specific analysis in mind.

We do not need to fear [bucket explosion][bu] and we can ask quite specific
question.

The `dynamic` bucket creation strategy is best suited for processing input where
we don't know what data are present, but the value range is fairly limited (to
avoid [bucket explosion][bu]).

--------------------------------------------------------------------------------

Static Bucket List Specification
--------------------------------------------------------------------------------

The `static` bucket creation happens when [histogram][h] is created and uses
*bucket specification* from [histogram][h] config. There are several ways that
the bucket list can be configured:

| format                 | name         | description                      |
| -                      | -            | -                                |
| `<b1>,<b2>,<b3>...`    | generic list | list of (assorted) buckets       |
| `<min>-<v1>-...-<max>` | range  list  | continuous list of range buckets |
| `<min>-<max>%<size>  ` | modulo list  | continuous list of range buckets |
| `<min>-<max>/<count>`  | divide list  | continuous list of range buckets |

*TODO:* Add logarithmic division of the interval into N buckets

#### generic list

The `generic list` provides most flexibility, but is the most tedious to setup.
It consists of comma separated list of bucket definitions and can mix and match
`value` and `range` buckets with ease.

> **Example:** Assuming web server log, we might use follwing buckets lists to
> categorize request types and status codes types:
>
> ````
> name=request; type=string; buckets=GET,POST,PUT,DELETE,PATCH
> name=status; type=int; buckets=100-200,200-300,300-400,400-500
> ````

Its most interesting feature is that it supports both `bucket` **and** `bucket
list` specification in the list. Which allows us to combine `generic list` with
other specification types.

> **Example:**  Sort hour field into before work-shift start, every hour of
> shift and after shift:
>
> ````
> name=hour; type=hour; buckets=0-9,9-17/1,17-24
> ````


#### range list

The `range list` provides easy way for splitting continuous range into buckets
of assorted size.

> **Example:**  Rewriting our hour vs work-shift example from `generic list`
> using `range list`
>
> ````
> name=hour; type=hour; buckets=0-9-10-11-12-13-14-15-16-17-24
> ````

#### modulo list

The `modulo list` provides splitting of `<min>-<max>` interval into slices
of **given** size. The **size** of the slice is given in the `<size>` field.

If the interval is not divisible by `<modulo>` value, then the last bucket will
be of differnt size.

> **Example:**  Sort hour field into `N` intervals. The example lists two
> ways to create buckets, one using `modulo` and one using `range list`
>
> ````
> name=hour; type=hour; buckets=0-24%4
> name=hour; type=hour; buckets=0-4-8-12-16-20-24
>
> name=hour; type=hour; buckets=0-24%7
> name=hour; type=hour; buckets=0-7-14-21-24
> ````

#### divide list

The `divide list` provides splitting of `<min>-<max>` interval into slices
of **equal** size. The **number** of slices is given in the `<count>` field.

If the interval is not divisible by `<count>` value, then each of the
be of differnt size.

> **Example:**  Sort hour field into `N` hour intervals. The example lists two
> ways to create buckets, one using `modulo` and one using `range list`
>
> ````
> name=hour; type=hour; buckets=0-24/4
> name=hour; type=hour; buckets=0-6-12-18-24
>
> name=hour; type=hour; buckets=0-24%7
> name=hour; type=hour; buckets=0-7-14-21-24
> ````


> **Example:**  Rewriting our hour vs work-shift example from `generic list`
> using `range list`
>
> ````
> name=hour; type=hour; buckets=0-9-10-11-12-13-14-15-16-17-24
> ````

--------------------------------------------------------------------------------

Dynamic Bucket List Specification
--------------------------------------------------------------------------------

The `dynamic` bucket list specification is more limited than the static one:


| format              | name     | description                                 |
| -                   | -        | -                                           |
|                     | identity |                                             |
| `<offset>+<length>` | slice    | only for types supporting `+, -` operations |

#### identity

The `identity` generation strategy is used for `value bucket` creation.

When value that does not match any existing bucket is encountered, the strategy
creates new `value bucket`, sorts it into bucket list and marks single occurence
into it.


#### slice

The `slice` generation strategy is used for `range bucket` creation.

When value that does not match any existing bucket is encountered, the strategy
uses *modulo by `<length>` offset by `<offset>`* to compute new `range bucket,
sorts it into existing bucket list and marks single occurence into it.

--------------------------------------------------------------------------------

Internal Bucket Types
--------------------------------------------------------------------------------

The `histosaur` has support for two more `bucket` types, but those are
considered internal as they need to be handled in special way during sample
matching.

Specifying them by hand could (and most likely) would result in bunch of garbage
in output. Internaly, they are/may be used for implementation of outlier
matching and storage

| name  | types | format        | notes                                   |
| -     | -     | -             | -                                       |
| more  | *all* | `<min>-`      | requires care when ordering buckets     |
| less  | *all* | `-<max>`      | requires care when ordering buckets     |
