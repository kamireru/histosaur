Histogram Specification
================================================================================

The [histogram][h] provides bunch of knobs and levers that can be tweak to get
us result we seek. This section provides overview of what they are and how they
can be changed.

[b]: ./bucket.md
[h]: ./histogram.md
[b]: ./value.md

--------------------------------------------------------------------------------

Default Histogram Specification
--------------------------------------------------------------------------------

The `histosaur` provides [histogram][h] with default specification for every
[regex][r] capture field existing in one or more configured regular
expression(s).

Please refer to [follow-up section](#histogram-specification) for specification
default values.

--------------------------------------------------------------------------------

Specification
--------------------------------------------------------------------------------

The [histogram][h] specification consists of zero or more config strings in
`key=value` form separated by semicolon `;`:

| full name   | shortcut | mandatory   | default    | supported |
| -           | -        |             | -          | -         |
| `field`     | `f`      | *mandatory* | *none*     | yes       |
| `type`      | `t`      | *mandatory* | `string`   | no        |
| `buckets`   | `b`      | *mandatory* | `identity` | no        |
| `title`     | `n`      |             | *field*    | yes       |
| `parser`    | `p`      |             | *none*     | no        |
| `transform` | `t`      |             | *none*     | no        |
| `group-by`  | `g`      |             | *none*     | no        |

#### field

The name of *regex capture group* whose content will be fed into histogram for
data parsing and sampling. The only really mandatory config `key`.

#### title

The [histogram][h] title rendered in final display and in interactive display
shell.

#### type

The [value type][v] into which `histosaur` will convert text captured by regexp
before any sampling is done.

Some value types (like `datetime` or `calendar`) may require additional
configuration for how to parse text into final storage type.

This field is mandatory, but defaults to `string` value type.

#### buckets

A static [bucket list][b] or name of dynamic bucket generation strategy.

This field is mandatory, but defaults to `dynamic, identity` bucket generation
strategy. Which fits nicely with default value for `type` config key. If `type`
key changes, it might be good idea to change bucket generation strategy too.

#### parser, transform, group-by

*TODO:* Section is work-in-progress

--------------------------------------------------------------------------------

Specification Examples
--------------------------------------------------------------------------------

Although `type` and `buckets` config keys are listed as mandatory, they do have
default value that will be used when left unspecified. Therefore the minimal
[histogram][h] specification looks like this:

> **Example:** All following are valid:
> ````
> field=hoge
> ````

Trailing semicolon and empty `key=value` pairs are allowed:

> **Example:** All following are valid:
>
> ````
> field=hoge;type=int
> field=hoge;type=int;
> field=hoge;;;;type=int;;;
> ````

Trailing and leading whitespace is allowed for both `key` and `value` part of
the specification, it will be trimmed automatically. This allows for adding
visual separation between pairs and increasing readability:

> **Example:** All following are valid:
> ````
> name=Hoge; type=int; buckets=1-2,3-4
> ````

Each configuration `key` has both `full name` and `shortcut`. The `shortcut` is
shorted prefix of the `full name` that can be used for identifying the `key` in
specification:

> **Example:** All following are valid:
> ````
> n=Hoge; t=int; b=1-2,3-4
> n=Hoge; t=int; bucket=1-2,3-4
> n=Hoge; t=int; group=kaga,piyo
> ````
