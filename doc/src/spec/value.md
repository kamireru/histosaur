Value Specification
================================================================================

This section provides overview of value types that the `histosaur` can use.

[bu]: ../trouble.md#bucket-explosion

--------------------------------------------------------------------------------

Basic Types
--------------------------------------------------------------------------------

The most basic types `histosaur` supports and probably easiest to use and
fastest to parse. They do not need any specialized parser configuration, just
plug and play:

| name       | examples                   |
| -          | -                          |
| `string`   | `"hogera"`, `"piyo"`       |
| `integer`  | `42`, `69`, `13`           |
| `float`    | `2.54`, `0.3048`, `0.4536` |

The `string` type is best suited for use with dynamically generated `value
buckets` as a way to explore data set at hand. Beware of [bucket explosion][bu]
though.

The `int` and `float` types are best suited with `range buckets`, no matter of
if generated statically or dynamically.

--------------------------------------------------------------------------------

Date/Time Types
--------------------------------------------------------------------------------

The next often used types are the ones dealing with date, time and duration.
They do require parser configuration and will generally be slower to extract
because of it.

| name       | examples                   |
| -          | -                          |
| `time`     | `12:00`, `6pm`, `5:32:06`  |
| `date`     | `2020-06-01`               |
| `datetime` | `2020-06-01 12:42:00`      |
| `duration` | `"1 week"`, `"3.45" secs"` |

Same as `int` and `float` basic types, the datetime types are best used with
`range buckets`.

--------------------------------------------------------------------------------

Calendar Types
--------------------------------------------------------------------------------

The calendar types are convenience names for combination of `datetime` parser
and subsequent split and extraction of resulting data into parts.

They require parser configuration same way `datetime` types do. Unlike, these
types are best used with `value bucket` since the number of values is small and
unlikely to cause [bucket explosion][bu].

| name           | storage   | range       | examples                 |
| -              | -         | -           | -                        |
| `hour`         | `integer` | `1` .. `24` |                          |
| `day`          | `integer` | `1` .. `7`  | `"Monday"`, `"Tue"`, `5` |
| `week`         | `integer` | `1` .. `52` |                          |
| `month`        | `integer` | `1` .. `12` | `"Jan"`, `"July"`, 7     |
| `year`         | `integer` |             |                          |
| `day_of_month` | `integer` | `1` .. `31` |                          |
| `day_of_year`  | `integer` | `1` .. `31` |                          |
