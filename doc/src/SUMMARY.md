Histosaur User Guide
================================================================================

- [Introduction](./introduction.md)
- [Get Started](./start.md)
- [Command Line Usage](./usage.md)
- [Examples](./examples.md)
- [Specification](./spec.md)
  - [Regex and Field](./spec/regex_and_field.md)
  - [Value](./spec/value.md)
  - [Bucket](./spec/bucket.md)
  - [Histogram](./spec/histogram.md)
- [Troubleshooting](./trouble.md)
