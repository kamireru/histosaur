use assert_cmd::Command;

#[test]
fn with_no_named_capture() {
    let output = indoc!{"
        >> Histogram: 'color' (buckets=3, marks=6)

        blue   1x  ( 16.67%) ########
        green  3x  ( 50.00%) #########################
        red    2x  ( 33.33%) ################


    "};

    Command::cargo_bin(crate::fixture::BINARY).unwrap()
        .args(&["-", "--regex", "^(?P<color>[a-z]+)$"])
        .write_stdin(crate::fixture::source::COLORS)
        .assert()
        .code(0)
        .stdout(output);
}
