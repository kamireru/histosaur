// Data 'file' containing list of RGB color names, each on separate line, for testing simple, text
// based histogram configurations.
pub static COLORS: &str = indoc!{r###"#
red
green
blue
red
green
green
"###};
