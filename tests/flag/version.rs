use assert_cmd::Command;

#[test]
fn with_long() {
    Command::cargo_bin(crate::fixture::BINARY).unwrap()
        .args(&["--version"])
        .assert()
        .code(0)
        .stdout(predicates::str::contains(crate::fixture::BINARY));
}

#[test]
fn with_short() {
    Command::cargo_bin(crate::fixture::BINARY).unwrap()
        .args(&["-V"])
        .assert()
        .code(0)
        .stdout(predicates::str::contains(crate::fixture::BINARY));
}
