use assert_cmd::Command;

#[test]
fn with_long() {
    Command::cargo_bin(crate::fixture::BINARY).unwrap()
        .args(&["--help"])
        .assert()
        .code(0)
        .stdout(predicates::str::contains("Usage"));
}

#[test]
fn with_short() {
    Command::cargo_bin(crate::fixture::BINARY).unwrap()
        .args(&["-h"])
        .assert()
        .code(0)
        .stdout(predicates::str::contains("Usage"));
}
