#[macro_use] extern crate indoc;


pub mod fixture {
    pub const BINARY: &str = "histosaur";

    pub mod source;
}

pub mod flag {
    pub mod help;
    pub mod version;
}

pub mod config {
    pub mod captures;
}
