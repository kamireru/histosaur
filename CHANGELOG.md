CHANGELOG
================================================================================

?.?.? (????-??-??)
--------------------------------------------------------------------------------

- add usage and examples into --help output
- add display of bucket count to histogram output
- add design documentation for cli config string format

0.1.0 (2020-09-02)
--------------------------------------------------------------------------------

- add multiple regex config via `--regex` option(s)
- add help display via `--help`, `--version` flags
- add processing (multiple) file sources via `<path>` argument
- add processing `stdin` source via `-` argument
- add basic histogram rendering
