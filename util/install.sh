#!/bin/bash -x

SOURCE=$(realpath "$0")
SOURCE=$(dirname $(dirname "$SOURCE"))

TARGET=${1:-$HOME}
TARGET=$(realpath "$TARGET");

cargo install --path "$SOURCE" --root "$TARGET"
