use std::collections::HashMap;

pub mod bucket;
pub mod hgram;
pub mod value;

pub use bucket::Bucket;
pub use hgram::Histogram;
pub use value::ParseToBuf;
pub use value::Value;

// -------------------------------------------------------------------------------------------------
// Count

/// Type alias for field storing mark count
///
pub type Count = u64;

// -------------------------------------------------------------------------------------------------
// Ratio

/// Type alias for percentages
pub type Ratio = f32;


// -------------------------------------------------------------------------------------------------
// HistogramMap

#[derive(Clone)]
pub struct HistogramMap(HashMap<String, Box<dyn Histogram>>);

impl HistogramMap {

    pub fn new() -> Self {
        Self(HashMap::default())
    }

    pub fn insert(&mut self, hgram: Box<dyn Histogram>) {
        if ! self.0.contains_key(hgram.name()) {
            self.0.insert(String::from(hgram.name()), hgram);
        }
    }


    pub fn mark(&mut self, name: &str, value: &str) {
        if let Some(hgram) = self.0.get_mut(name) {
            hgram.mark(value);
        }
    }

    pub fn iter(&self) -> std::collections::hash_map::Iter<String, Box<dyn Histogram>> {
        self.0.iter()
    }

    pub fn iter_mut(&mut self) -> std::collections::hash_map::IterMut<String, Box<dyn Histogram>> {
        self.0.iter_mut()
    }
}

impl std::iter::Extend<Box<dyn Histogram>> for HistogramMap {
    fn extend<I: IntoIterator<Item=Box<dyn Histogram>>>(&mut self, iter: I) {
        iter.into_iter().fold(self, |memo, h| {
            memo.insert(h);
            memo
        });
    }
}

impl std::iter::IntoIterator for HistogramMap {
    type Item     = (String, Box<dyn Histogram>);
    type IntoIter = std::collections::hash_map::IntoIter<String, Box<dyn Histogram>>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
