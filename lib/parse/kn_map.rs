// -------------------------------------------------------------------------------------------------
// KeyNameMap

/// Storage for mapping between key name and internal representation
///
/// The type stores mapping between key name textual and internal representations. It provides
/// method for finding the internal representation according to either exact or prefix match of the
/// textual one.
///
/// This is a helper type used for parsing list of [`KvPair`] instances into instances of some
/// internal config type that will be easier to handle programatically than arbitrary strings.
///
/// [`KvPair`]: crate::parse::KvPair
///
#[derive(Debug)]
pub struct KeyNameMap<'n, N>
where
    N: std::fmt::Debug + Copy
{
    list: Vec<KeyNamePair<'n, N>>,
}

impl<'n, N> KeyNameMap<'n, N>
where
    N: std::fmt::Debug + Copy
{
    pub fn find(&self, key: &str) -> Result<N, KeyLookupError> {
        let mut count = 0;
        let mut found: Option<&KeyNamePair<N>> = None;

        for pair in self.list.iter() {
            // if we have found exact match, we don't care if the match may be prefix of longer key
            // name and return it right away.
            if pair.key == key {
                return Ok(pair.name);
            }
            else if pair.key.starts_with(key) {
                count += 1;
                found  = Some(pair);
            }
        }

        match (found, count) {
            (Some(pair), 1) => Ok(pair.name),
            (Some(_), cnt)  => Err(KeyLookupError::Ambiguous(key.to_owned(), cnt)),
            (None, _)       => Err(KeyLookupError::NotSupported(key.to_owned())),
        }
    }
}

impl<'n, N> std::convert::From<Vec<(&'n str, N)>> for KeyNameMap<'n, N>
where
    N: std::fmt::Debug + Copy
{
    fn from(list: Vec<(&'n str, N)>) -> Self {
        let list = list.into_iter()
            .map(|p| KeyNamePair { key: p.0, name: p.1})
            .collect();
        Self { list }
    }
}


// -------------------------------------------------------------------------------------------------
// KeyNamePair

/// Storage for mapping between key textual name and internal representation (usualy `enum`
/// variant).
///
/// The type is internal to the module since it does not need to be instantiated
/// and fed into [`KeyNameMap`] manually. The intended use is to create it in one go from vector of
/// tupples.
///
#[derive(Debug)]
struct KeyNamePair<'n, N>
where
    N: std::fmt::Debug + Copy
{
    key: &'n str,
    name: N,
}


// -------------------------------------------------------------------------------------------------
// KeyLookupError

/// Representation of errors that can be encountered when checking if given string `key` maps to
/// some kind of internal representation stored in [`KeyNameMap`]
///
#[derive(Debug,PartialEq,Clone)]
pub enum KeyLookupError {

    /// Occurs when lookup attempt finds more than one mapping. This may happen only when *key* is
    /// a prefix of multiple mapping and at the same time it does not match any mapping exactly.
    Ambiguous(String, u8),

    /// Occurs when lookup attempt finds no mapping for given `key` - ie invalid key.
    NotSupported(String),
}

impl std::error::Error for KeyLookupError {
}

impl std::fmt::Display for KeyLookupError {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Ambiguous(key, count) =>
                write!(fmt, "key ambiguous (key='{}', matches={})", key, count),

            Self::NotSupported(key) =>
                write!(fmt, "key not supported (key={})", key),
        }
    }
}


// -------------------------------------------------------------------------------------------------
// tests

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug,PartialEq,Clone,Copy)]
    enum Name {
        Hogera,
        Kagera,
        Hoge,
        Kaga,
    }

    lazy_static! {
        static ref KEY_NAME_MAP: KeyNameMap<'static, Name> = {
            KeyNameMap::from(vec![
                ("hoge",   Name::Hoge),
                ("hogera", Name::Hogera),
                ("kaga",   Name::Kaga),
                ("kagera", Name::Kagera),
            ])
        };
    }

    mod find {
        use super::*;

        #[test]
        fn ok_found_prefix() {
            assert_eq!(Ok(Name::Kagera), KEY_NAME_MAP.find("kage"));
        }

        #[test]
        fn ok_found_exact() {
            assert_eq!(Ok(Name::Kagera), KEY_NAME_MAP.find("kagera"));
        }

        #[test]
        fn err_ambiguous() {
            assert_matches!(KEY_NAME_MAP.find("kag"), Err(KeyLookupError::Ambiguous(_, 2)));
            assert_matches!(KEY_NAME_MAP.find(""),    Err(KeyLookupError::Ambiguous(_, 4)));
        }

        #[test]
        fn err_not_found() {
            assert_matches!(KEY_NAME_MAP.find("piyo"), Err(KeyLookupError::NotSupported(_)));
        }
    }
}
