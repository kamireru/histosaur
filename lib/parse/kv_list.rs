use std::str::pattern::Pattern;

// -------------------------------------------------------------------------------------------------
// KvPair

/// Store references to `key` and `value` string slices of original text that represent single
/// `key=value` pair.
///
/// The type only stores references to string slices, it does not do any validation nor on `key`
/// nor on `value` part. One or both parts of the pair can even be reference to empty slice.
///
/// Any restrictions regarding `key` format need to be handled in different abstraction. See
/// [`KeyNameMap`] for more information about that.
///
/// [`KeyNameMap`]: crate::parse::KeyNameMap
///
#[derive(Debug,PartialEq,Clone,Copy)]
pub struct KvPair<'a> {
    pub key: &'a str,
    pub val: &'a str
}

// -------------------------------------------------------------------------------------------------
// SplitKvList

/// Store state for key-value interator using two patterns
///
/// The struct provides storage for configuration and state of the [`Iterator`] implementation that
/// splits string into key-value pairs. The `outer pattern` is used for splitting string into
/// key-value pairs and `inner pattern` is used for splitting pair into key and value components.
///
pub struct SplitKvList<'i, O, I>
where
    O: Pattern<'i> + Copy,
    I: Pattern<'i> + Copy,
{
    spliter: std::str::Split<'i, O>,
    inner_pattern: I,
}

impl<'i, O, I> SplitKvList <'i, O, I>
where
    O: Pattern<'i> + Copy,
    I: Pattern<'i> + Copy,
{

    pub fn new(text: &'i str, outer_pattern: O, inner_pattern: I) -> Self {
        Self {
            spliter: text.split(outer_pattern),
            inner_pattern,
        }
    }
}

impl<'i, O, I> Iterator for SplitKvList<'i, O, I>
where
    O: Pattern<'i> + Copy,
    I: Pattern<'i> + Copy,
{
    type Item = KvPair<'i>;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(pair) = self.spliter.next() {
            let mut iter = pair.splitn(2, self.inner_pattern);

            match (iter.next(), iter.next()) {
                (Some(key), Some(val)) => {
                    let key = key.trim();
                    let val = val.trim();

                    if !key.is_empty() || ! val.is_empty() {
                        return Some(KvPair {
                            key: key.trim(),
                            val: val.trim(),
                        })
                    }
                },

                (Some(key), None) => {
                    let key = key.trim();

                    if ! key.is_empty() {
                        return Some(KvPair {
                            key,
                            val: "",
                        });
                    }
                },

                // we can never get into this branch since split will always return at least one
                // element (whole string) even if that is empty
                (None, _) => unreachable!(),
            }
        }
        None
    }
}

// -------------------------------------------------------------------------------------------------
// tests

#[cfg(test)]
mod tests {
    use super::*;

    mod split_kv_list {
        use super::*;

        #[test]
        fn empty() {
            assert_eq!(
                vec![] as Vec<KvPair>,
                SplitKvList::new("", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn empty_pair() {
            assert_eq!(
                vec![] as Vec<KvPair>,
                SplitKvList::new(";;;;", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn empty_pair_with_separator() {
            assert_eq!(
                vec![] as Vec<KvPair>,
                SplitKvList::new("=", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn present_key() {
            assert_eq!(
                vec![
                    KvPair { key: "key", val: "" },
                ],
                SplitKvList::new("key=", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn present_val() {
            assert_eq!(
                vec![
                    KvPair { key: "", val: "val" },
                ],
                SplitKvList::new("=val", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn present_key_val() {
            assert_eq!(
                vec![
                    KvPair { key: "key", val: "val" },
                ],
                SplitKvList::new("key=val", ';', '=').collect::<Vec<_>>(),
            );
        }

        #[test]
        fn present_key_val_with_multiple_separators() {
            assert_eq!(
                vec![
                    KvPair { key: "key", val: "v=a=l" },
                ],
                SplitKvList::new("key=v=a=l", ';', '=').collect::<Vec<_>>(),
            );
        }
    }
}
