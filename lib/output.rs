use crate::store::Count;
use crate::store::Ratio;

// -------------------------------------------------------------------------------------------------
// Histogram

#[derive(Debug,Clone)]
pub struct Histogram {
    label: String,
    meta: Metadata,
    list: Data,
}

impl Histogram {
    pub fn new<S: Into<String>>(label: S) -> Self {
        Self {
            label: label.into(),
            meta: Metadata::default(),
            list: Data::default(),
        }
    }

    pub fn push(&mut self, point: Point) {
        self.meta.update_from(&point);
        self.list.push(point);
    }
}

impl std::fmt::Display for Histogram {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, ">> Histogram: '{}' ({})", self.label, self.meta)?;
        writeln!(f)?;
        writeln!(f, "{}", self.list)
    }
}

// -------------------------------------------------------------------------------------------------
// Metadata

#[derive(Debug,Default,Clone)]
struct Metadata {
    buckets: Count,
    marks: Count,
}

impl Metadata {
    pub fn update_from(&mut self, point: &Point) {
        self.buckets += 1;
        self.marks   += point.count();
    }
}

impl std::fmt::Display for Metadata {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "buckets={}, marks={}", self.buckets, self.marks)
    }
}

// -------------------------------------------------------------------------------------------------
// Data

#[derive(Debug,Default,Clone)]
pub struct Data {

    /// store maximum width of any [`Point`] label
    width_label: usize,

    /// store maximum number of digits of any [`Point`] count
    width_count: usize,

    /// total number of marks to display
    marks: Count,

    points: Vec<Point>,
}

impl Data {
    pub fn push(&mut self, point: Point) {
        self.width_label = std::cmp::max(self.width_label, point.label.len());
        self.width_count = std::cmp::max(self.width_count, digits(point.count));

        self.marks += point.count;

        self.points.push(point);
    }
}

impl std::fmt::Display for Data {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for point in &self.points {
            writeln!(
                f,
                "{label:<label_width$}  {count:>count_width$}x  ({ratio:>6.2}%) {graph:#>graph_width$}",
                label = point.label(),
                count = point.count(),
                ratio = point.ratio(self.marks),
                graph = "",

                label_width = self.width_label,
                count_width = self.width_count,
                graph_width = point.ratio_bar_len(self.marks),
            )?;
        }
        Ok(())
    }
}

// -------------------------------------------------------------------------------------------------
// Point

// store label width, count decimals/width for formatting?
#[derive(Debug,Clone)]
pub struct Point {
    pub label: String,
    pub count: Count,
}

impl Point {
    pub fn new<S: Into<String>>(label: S, count: Count) -> Self {
        Self {
            label: label.into(),
            count,
        }
    }

    pub fn label(&self) -> &str {
        &self.label
    }

    pub fn count(&self) -> Count {
        self.count
    }

    pub fn ratio(&self, total_count: Count) -> Ratio {
        (self.count * 100) as Ratio / total_count as Ratio
    }

    /// Compute length of the [`Point`] graph bar from mark count in instance and total mark count
    /// passed into method from outside.
    fn ratio_bar_len(&self, total_count: Count) -> usize {
        ((self.count * 100) / (total_count * 2)) as usize
    }
}

// -------------------------------------------------------------------------------------------------
// digits()

fn digits(number: Count) -> usize {
    let mut number = number;
    let mut digits = 1;

    loop {
        number /= 10;
        if number > 0 {
            digits += 1;
        }
        else {
            return digits;
        }
    }
}

// -------------------------------------------------------------------------------------------------
// tests

#[cfg(test)]
mod digits {

    #[test]
    fn count() {
        assert_eq!(1, super::digits(0));
        assert_eq!(1, super::digits(1));
        assert_eq!(1, super::digits(9));
        assert_eq!(2, super::digits(10));
        assert_eq!(2, super::digits(19));
        assert_eq!(2, super::digits(99));
        assert_eq!(3, super::digits(100));
    }
}
