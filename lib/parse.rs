use std::fs;
use std::io;
use std::io::BufRead;
use std::path::PathBuf;

use regex::Regex;

use crate::store::HistogramMap;

mod kn_map;
mod kv_list;

pub use kn_map::*;
pub use kv_list::*;

// -------------------------------------------------------------------------------------------------
// Matcher

/// [`Matcher`] holds refs to [`Regex`] instances used for parsing input text and also instance to
/// store found result into. We assume that single [`Matcher`] instance will operate in single
/// thread, returning accumulated, partial results in type that allows merging.
///
#[derive(Debug)]
pub struct Matcher<'r> {
    regexes: &'r [Regex],
    buffer: String,
}

impl<'r> Matcher<'r> {
    pub fn new(regexes: &'r [Regex]) -> Self {
        Self {
            regexes,
            buffer: String::with_capacity(256),
        }
    }

    pub fn process_source_into(&mut self, source: Source, hgramap: &mut HistogramMap) -> io::Result<()> {
        let mut reader = io::BufReader::new(source.open()?);

        while 0 < reader.read_line(&mut self.buffer)? {
            self.process_buffer(hgramap);
            self.buffer.clear()
        }

        Ok(())
    }

    pub fn process_buffer(&mut self, hgramap: &mut HistogramMap) {
        if self.buffer.ends_with('\n') {
            self.buffer.pop();
            if self.buffer.ends_with('\r') {
                self.buffer.pop();
            }
        }

        for regex in self.regexes {
            for captures in regex.captures_iter(self.buffer.as_ref()) {
                for name in regex.capture_names().filter_map(std::convert::identity) {
                    if let Some(value) = captures.name(name) {
                        hgramap.mark(name, value.as_str());
                    }
                }
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------
// Source

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum Source {
    Stdin,
    Path(PathBuf),
}

impl Source {

    /// Consume self and return [`io::Read`] trait object
    pub fn open(self) -> io::Result<Box<dyn io::Read>> {
        match self {
            Self::Stdin =>
                Ok(Box::new(io::stdin())),

            Self::Path(path) =>
                Ok(Box::new(fs::File::open(path)?)),
        }
    }
}

impl std::convert::From<&str> for Source {
    fn from(source: &str) -> Source {
        match source {
            "-" => Source::Stdin,
            txt => Source::Path(PathBuf::from(txt)),
        }
    }
}

impl std::fmt::Display for Source {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Stdin      => write!(fmt, "<STDIN>"),
            Self::Path(path) => write!(fmt, "{}", path.to_string_lossy()),
        }
    }
}
