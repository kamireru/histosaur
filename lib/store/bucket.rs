use crate::store::Value;

// -------------------------------------------------------------------------------------------------
// Bucket

/// Trait providing abstraction over [`Bucket`] types
pub trait Bucket<V: Value>: BucketClone<V> {

    /// Return user readable name of the [`Bucket`]
    fn name(&self) -> &str;

    /// Check if `sample` fits into [`Bucket`]
    fn compare(&self, sample: &V) -> std::cmp::Ordering;
}

impl<V: Value> Clone for Box<dyn Bucket<V>> {
    fn clone(&self) -> Box<dyn Bucket<V>> {
        self.clone_box()
    }
}

// -------------------------------------------------------------------------------------------------
// BucketClone

/// Helper trait to allow cloning of trait objects
///
/// See [How to clone a struct storing a boxed trait object?](
/// https://stackoverflow.com/questions/30353462/how-to-clone-a-struct-storing-a-boxed-trait-object)
pub trait BucketClone<V> {
    fn clone_box(&self) -> Box<dyn Bucket<V>>;
}

impl<T, V: Value> BucketClone<V> for T
where
    T: Bucket<V> + Clone + 'static,
{
    fn clone_box(&self) -> Box<dyn Bucket<V>> {
        Box::new(self.clone())
    }
}


// -------------------------------------------------------------------------------------------------
// EqBucket

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone)]
pub struct EqBucket<V: Value> {
    name: String,
    value: V,
}

impl<V: Value> EqBucket<V> {

    pub fn new(value: V) -> Self {
        Self {
            name: format!("{}", value),
            value,
        }
    }
}

impl<V: Value + 'static> Bucket<V> for EqBucket<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn compare(&self, sample: &V) -> std::cmp::Ordering {
        if self.value.eq(sample) {
            std::cmp::Ordering::Equal
        }
        else if self.value.lt(&sample) {
            std::cmp::Ordering::Less
        }
        else {
            std::cmp::Ordering::Greater
        }
    }
}

// -------------------------------------------------------------------------------------------------
// LtBucket

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone)]
pub struct LtBucket<V: Value> {
    name: String,
    value: V,
}

impl<V: Value> LtBucket<V> {

    pub fn new(value: V) -> Self {
        Self {
            name: format!("? < {}", value),
            value,
        }
    }
}

impl<V: Value + 'static> Bucket<V> for LtBucket<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn compare(&self, sample: &V) -> std::cmp::Ordering {
        if self.value.lt(&sample) {
            std::cmp::Ordering::Equal
        }
        else {
            std::cmp::Ordering::Greater
        }
    }
}

// -------------------------------------------------------------------------------------------------
// GeBucket

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone)]
pub struct GeBucket<V: Value> {
    name: String,
    value: V,
}

impl<V: Value> GeBucket<V> {

    pub fn new(value: V) -> Self {
        Self {
            name: format!("? >= {}", value),
            value,
        }
    }
}

impl<V: Value + 'static> Bucket<V> for GeBucket<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn compare(&self, sample: &V) -> std::cmp::Ordering {
        if self.value.lt(&sample) {
            std::cmp::Ordering::Less
        }
        else {
            std::cmp::Ordering::Equal
        }
    }
}

// -------------------------------------------------------------------------------------------------
// RangeBucket

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone)]
pub struct RangeBucket<V: Value> {
    name: String,
    lower: V,
    upper: V,
}

impl<V: Value> RangeBucket<V> {

    pub fn new(lower: V, upper: V) -> Self {
        Self {
            name: format!("{} <= ? < {}", lower, upper),
            lower,
            upper,
        }
    }
}

impl<V: Value + 'static> Bucket<V> for RangeBucket<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn compare(&self, sample: &V) -> std::cmp::Ordering {
        if self.lower.lt(&sample) {
            std::cmp::Ordering::Less
        }
        else if self.upper.ge(&sample) {
            std::cmp::Ordering::Greater
        }
        else {
            std::cmp::Ordering::Equal
        }
    }
}

// -------------------------------------------------------------------------------------------------
// AnyBucket

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone)]
pub struct AnyBucket<V: Value> {
    name: String,
    mark: std::marker::PhantomData<V>,
}

impl<V: Value> AnyBucket<V> {

    pub fn new() -> Self {
        Self {
            name: "anything".to_string(),
            mark: std::marker::PhantomData,
        }
    }
}

impl<V: Value + 'static> Bucket<V> for AnyBucket<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn compare(&self, _sample: &V) -> std::cmp::Ordering {
        std::cmp::Ordering::Equal
    }
}
