use crate::store::Count;
use crate::store::Value;
use crate::store::bucket::EqBucket;
use crate::store::bucket::Bucket;

use crate::output;

// -------------------------------------------------------------------------------------------------
// Histogram

pub trait Histogram: HistogramClone {

    /// Return user readable name of the [`Histogram`]
    fn name(&self) -> &str;

    /// Add mark for `sample` into [`Histogram`]
    fn mark(&mut self, sample: &str);

    fn output(&self) -> output::Histogram;
}

impl Clone for Box<dyn Histogram> {
    fn clone(&self) -> Box<dyn Histogram> {
        self.clone_box()
    }
}

// -------------------------------------------------------------------------------------------------
// HistogramClone

pub trait HistogramClone {
    fn clone_box(&self) -> Box<dyn Histogram>;
}

impl<T> HistogramClone for T
where
    T: Histogram + Clone + 'static,
{
    fn clone_box(&self) -> Box<dyn Histogram> {
        Box::new(self.clone())
    }
}

// -------------------------------------------------------------------------------------------------
// DynamicHistogram


/// Storage for histogram with dynamicaly created buckets
///
/// The [`DynamicHistogram`] type implements histogram storage with [`Bucket`]s that are generated
/// dymically from samples that don't fit into any of the existing ones.
///
/// TODO: We implement only [`EqBucket`] creation, which is basically 'identity' bucket generation
/// strategy. We may implement more strategies in future.
///
#[derive(Clone)]
pub struct DynamicHistogram<V: Value> {
    name: String,

    buffer: V,
    buckets: Vec<(Box<dyn Bucket<V>>, Count)>,

    /// total number of marks in histogram
    marks: Count,

    /// maximum number of marks for single bucket
    maxim: Count,
    error: Count,
}

impl<V: Value> DynamicHistogram<V> {
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),

            buffer: V::default(),
            buckets: Vec::new(),

            marks: 0,
            maxim: 0,
            error: 0,
        }
    }
}

impl<V: Value + 'static> Histogram for DynamicHistogram<V> {

    fn name(&self) -> &str {
        &self.name
    }

    fn mark(&mut self, sample: &str) {
        // we assume that everything found will get sorted into bucket
        self.marks += 1;

        if V::parse_to_buf(&mut self.buffer, sample).is_err() {
            self.error += 1;
            return;
        }

        match self.buckets.binary_search_by(|p| p.0.compare(&self.buffer)) {
            Ok(idx)  => {
                self.buckets[idx].1 += 1;
                self.maxim = std::cmp::max(self.maxim, self.buckets[idx].1);
            }
            Err(idx) => {
                let bucket = EqBucket::<V>::new(self.buffer.clone());
                self.buckets.insert(idx, (Box::new(bucket), 1));
                self.maxim = std::cmp::max(self.maxim, 1);
            },
        }
    }

    fn output(&self) -> output::Histogram {
        let mut output = output::Histogram::new(self.name());
        for bucket in &self.buckets {
            let point  = output::Point::new(
                bucket.0.name(),
                bucket.1,
            );
            output.push(point);
        }
        output
    }
}
