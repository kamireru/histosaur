// -------------------------------------------------------------------------------------------------
// Value

/// Marker trait representing `value` which can be sorted into [`Bucket`]
///
/// The requirements are plentiful but also logical:
///
/// - [`PartialEq`] + [`Eq`] checking if sample equals to [`Bucket`] value
/// - [`PartialOrd`] + [`Ord`] for sorting [`Bucket`] list to speed up lookups
/// - [`Display`] for displaying [`Bucket`] when rendering histogram
/// - [`ParseToBuf`] for parsing `&str` into [`Value`]
/// - [`Default`] for initializing buffers used by `[ParseToBuf`]
///
/// Since the [`Ord`] trait requires [`Eq`] + [`PartialOrd`], we can explicitly require in trait
/// bound only [`Ord`] and letting rest of them to be required implicitly.
///
/// [`Bucket`]: crate::store::Bucket
///
/// [`Display`]: std::fmt::Display
/// [`Eq`]: std::cmp::PartialEq
/// [`Ord`]: std::cmp::PartialOrd
/// [`PartialEq`]: std::cmp::PartialEq
/// [`PartialOrd`]: std::cmp::PartialOrd
///
pub trait Value: Ord + Clone + ParseToBuf + std::default::Default + std::fmt::Display {}

impl<T> Value for T where T: Ord + Clone + ParseToBuf + std::default::Default + std::fmt::Display {}

// -------------------------------------------------------------------------------------------------
// ParseToBuf

/// Parse `str` into type and store result into buffer
///
/// We are defining custom `str` parsing trait because we need special handling for values from
/// capture groups. The main reason we can't use [`From`](std::convert::From) trait is because we
/// need to be able to *'parse'* `&str` values into [`String`] without allocating new memory.
///
/// The data processing loop will pass `&str` value to a histogram that needs to parse it into `V`
/// and pass that into code checking into which bucket it belongs. The issue is that if we allocate
/// every time we are checking value, we could slow our code considerably.
///
/// The [`ParseToBuf`] trait tries to solve this issue by parsing into existing, reusable buffer.
/// There will most likely be no speedup for primitive types like numbers, but we should be able to
/// get some speed-up for text.
///
pub trait ParseToBuf {
    type Err;

    fn parse_to_buf(buf: &mut Self, s: &str) -> Result<(), Self::Err>;
}

impl ParseToBuf for String {
    type Err = std::convert::Infallible;

    fn parse_to_buf(buf: &mut Self, s: &str) -> Result<(), Self::Err> {
        buf.clear();
        buf.push_str(s);
        Ok(())
    }
}

impl ParseToBuf for i32 {
    type Err = <Self as std::str::FromStr>::Err;

    fn parse_to_buf(buf: &mut Self, s: &str) -> Result<(), Self::Err> {
        *buf = s.parse::<Self>()?;
        Ok(())
    }
}

#[cfg(test)]
mod parse_to_buf {
    use super::*;

    #[test]
    fn i32() {
        let mut buffer: i32 = 0;

        i32::parse_to_buf(&mut buffer, "8").unwrap();
        assert_eq!(8, buffer);
    }

    #[test]
    fn string() {
        let mut buffer: String = String::new();

        String::parse_to_buf(&mut buffer, "hoge").unwrap();
        assert_eq!(String::from("hoge"), buffer);
    }
}
