#![feature(pattern)]

#![allow(clippy::new_without_default)]

#[macro_use]
extern crate lazy_static;

#[cfg_attr(test, macro_use)]
extern crate assert_matches;

pub mod parse;
pub mod config;
pub mod store;
pub mod output;
