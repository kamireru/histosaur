use crate::config;
use crate::parse::KvPair;
use crate::parse::SplitKvList;

// -------------------------------------------------------------------------------------------------
// Outline

#[derive(Debug,Clone)]
pub struct Outline {
    pub title: Option<String>,
    pub name: String,
}

impl Outline {
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            title: None,
            name: name.into(),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn title(&self) -> &str {
        self.title.as_ref().unwrap_or(&self.name)
    }
}

impl std::str::FromStr for Outline {
    type Err = config::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use std::convert::TryFrom;

        let pairs: Vec<KvPair> = SplitKvList::new(s, ';', '=').collect();
        Self::try_from(pairs)
    }
}


impl std::convert::TryFrom<OutlineBuilder> for Outline {
    type Error = config::Error;

    fn try_from(builder: OutlineBuilder) -> Result<Self, Self::Error> {
        builder.build().map_err(Into::into)
    }
}

impl<'a> std::convert::TryFrom<Vec<KvPair<'a>>> for Outline {
    type Error = config::Error;

    fn try_from(list: Vec<KvPair<'a>>) -> Result<Self, Self::Error> {
        let builder = OutlineBuilder::try_from(list)?;
        Self::try_from(builder)
    }
}


// -------------------------------------------------------------------------------------------------
// OutlineBuilder


/// The main means of creating the [`Outline`] instance is through [`FromStr`] trait. But since the
/// creation is quite complex, we also provide builder type that constructs the [`Outline`]
/// from pre-parsed key-value fields.
///
/// [`FromStr`]: std::str::FromStr
///
pub struct OutlineBuilder {
    name: Option<String>,
    title: Option<String>,
}

impl OutlineBuilder {

    pub fn new() -> Self {
        Self {
            name: None,
            title: None,
        }
    }

    pub fn name(&mut self, name: impl Into<String>) -> &Self {
        self.name = Some(name.into());
        self
    }

    pub fn title(&mut self, title: impl Into<String>) -> &Self {
        self.title = Some(title.into());
        self
    }

    pub fn build(self) -> Result<Outline, config::Error> {
        Ok(Outline {
            name: self.name.ok_or_else(|| {
                config::Error::required_key_absent("name").field()
            })?,
            title: self.title,
        })
    }
}

impl<'a> std::convert::TryFrom<Vec<KvPair<'a>>> for OutlineBuilder {
    type Error = config::Error;

    fn try_from(list: Vec<KvPair<'a>>) -> Result<Self, Self::Error> {
        let mut builder = OutlineBuilder::new();
        for pair in list {
            let key = FIELD_KEY_MAP.find(pair.key)
                .map_err(|e| config::Error::from(e).field())?;

            match key {
                FieldCfgKey::Name  => builder.name(pair.val),
                FieldCfgKey::Title => builder.title(pair.val),
            };
        }
        Ok(builder)
    }
}

// -------------------------------------------------------------------------------------------------
// FieldCfgKey

#[derive(Debug,PartialEq,Clone,Copy)]
enum FieldCfgKey {
    Title,
    Name,
}

lazy_static! {
    static ref FIELD_KEY_MAP: crate::parse::KeyNameMap<'static, FieldCfgKey> = {
        crate::parse::KeyNameMap::from(vec![
            ("title", FieldCfgKey::Title),
            ("name",  FieldCfgKey::Name),
        ])
    };
}

// -------------------------------------------------------------------------------------------------
// tests

#[cfg(test)]
mod outline {
    use super::*;

    mod from_str {
        use super::*;
        use std::str::FromStr;

        #[test]
        fn with_key_unknown() {
            let error = Outline::from_str("unknown=").unwrap_err();
            assert_matches!(error.kind(), config::ErrorKind::InvalidSpecFormat);
        }

        #[test]
        fn with_name() {
            let outline = Outline::from_str("name=NAME").unwrap();

            assert_eq!(String::from("NAME"), outline.name);
            assert_eq!(None, outline.title);
        }
    }

    mod try_from {
        use super::*;
        use std::convert::TryFrom;

        mod kv_pair_vec {
            use super::*;

            #[test]
            fn with_key_unknown() {
                let error = Outline::try_from(vec![
                    KvPair { key: "unknown", val: "UNKNOWN" },
                ]).unwrap_err();

                assert_matches!(error.kind(), config::ErrorKind::InvalidSpecFormat);
            }

            #[test]
            fn with_name() {
                let outline = Outline::try_from(vec![
                    KvPair { key: "name", val: "NAME" },
                ]).unwrap();

                assert_eq!(String::from("NAME"), outline.name);
                assert_eq!(None, outline.title);
            }

            #[test]
            fn with_name_absent() {
                let error = Outline::try_from(vec![
                    KvPair { key: "title", val: "TITLE" },
                ]).unwrap_err();

                assert_matches!(error.kind(), config::ErrorKind::RequiredKeyAbsent(_));
            }

            #[test]
            fn with_title() {
                let outline = Outline::try_from(vec![
                    KvPair { key: "name", val: "NAME" },
                    KvPair { key: "title", val: "TITLE" },
                ]).unwrap();

                assert_eq!(Some(String::from("TITLE")), outline.title);
            }
        }
    }
}
