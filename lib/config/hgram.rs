use crate::config;
use crate::parse::KvPair;
use crate::parse::SplitKvList;

// -------------------------------------------------------------------------------------------------
// Outline

#[derive(Debug,Clone)]
pub struct Outline {
    pub title: Option<String>,
    pub field: String,
}

impl Outline {
    pub fn new(field: impl Into<String>) -> Self {
        Self {
            title: None,
            field: field.into(),
        }
    }

    pub fn field(&self) -> &str {
        &self.field
    }

    pub fn title(&self) -> &str {
        self.title.as_ref().unwrap_or(&self.field)
    }
}

impl std::str::FromStr for Outline {
    type Err = config::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use std::convert::TryFrom;

        let pairs: Vec<KvPair> = SplitKvList::new(s, ';', '=').collect();
        Self::try_from(pairs)
    }
}


impl std::convert::TryFrom<OutlineBuilder> for Outline {
    type Error = config::Error;

    fn try_from(builder: OutlineBuilder) -> Result<Self, Self::Error> {
        builder.build().map_err(Into::into)
    }
}

impl<'a> std::convert::TryFrom<Vec<KvPair<'a>>> for Outline {
    type Error = config::Error;

    fn try_from(list: Vec<KvPair<'a>>) -> Result<Self, Self::Error> {
        let builder = OutlineBuilder::try_from(list)?;
        Self::try_from(builder)
    }
}

impl std::convert::From<config::field::Outline> for Outline {
    fn from(field: config::field::Outline) -> Self {
        Self {
            title: field.title,
            field: field.name,
        }
    }
}


// -------------------------------------------------------------------------------------------------
// OutlineBuilder


/// The main means of creating the [`Outline`] instance is through [`FromStr`] trait. But since the
/// creation is quite complex, we also provide builder type that constructs the [`Outline`]
/// from pre-parsed key-value fields.
///
/// [`FromStr`]: std::str::FromStr
///
pub struct OutlineBuilder {
    field: Option<String>,
    title: Option<String>,
}

impl OutlineBuilder {

    pub fn new() -> Self {
        Self {
            field: None,
            title: None,
        }
    }

    pub fn field(&mut self, field: impl Into<String>) -> &Self {
        self.field = Some(field.into());
        self
    }

    pub fn title(&mut self, title: impl Into<String>) -> &Self {
        self.title = Some(title.into());
        self
    }

    pub fn build(self) -> Result<Outline, config::Error> {
        Ok(Outline {
            field: self.field.ok_or_else(|| {
                config::Error::required_key_absent("field").hgram()
            })?,
            title: self.title,
        })
    }
}

impl<'a> std::convert::TryFrom<Vec<KvPair<'a>>> for OutlineBuilder {
    type Error = config::Error;

    fn try_from(list: Vec<KvPair<'a>>) -> Result<Self, Self::Error> {
        let mut builder = OutlineBuilder::new();
        for pair in list {
            let key = FIELD_KEY_MAP.find(pair.key)
                .map_err(|e| config::Error::from(e).field())?;

            match key {
                HgramCfgKey::Field => builder.field(pair.val),
                HgramCfgKey::Title => builder.title(pair.val),
            };
        }
        Ok(builder)
    }
}

// -------------------------------------------------------------------------------------------------
// HgramCfgKey

#[derive(Debug,PartialEq,Clone,Copy)]
enum HgramCfgKey {
    Title,
    Field,
}

lazy_static! {
    static ref FIELD_KEY_MAP: crate::parse::KeyNameMap<'static, HgramCfgKey> = {
        crate::parse::KeyNameMap::from(vec![
            ("title",   HgramCfgKey::Title),
            ("field",   HgramCfgKey::Field),
        ])
    };
}
