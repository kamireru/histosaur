use crate::parse::KeyLookupError;

// -------------------------------------------------------------------------------------------------
// Error

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    spec: Option<SpecKind>,
    source: Option<Box<dyn std::error::Error + Sync + Send>>,
}

impl Error {

    pub fn required_key_absent(key: impl Into<String>) -> Self {
        Self {
            kind: ErrorKind::RequiredKeyAbsent(key.into()),
            spec: None,
            source: None,
        }
    }

    pub fn required_capture_absent(name: impl Into<String>) -> Self {
        Self {
            kind: ErrorKind::RequiredCaptureAbsent(name.into()),
            spec: Some(SpecKind::Field),
            source: None,
        }
    }

    pub fn required_field_absent(name: impl Into<String>) -> Self {
        Self {
            kind: ErrorKind::RequiredFieldAbsent(name.into()),
            spec: Some(SpecKind::Hgram),
            source: None,
        }
    }

    pub fn duplicate_field_spec(name: impl Into<String>) -> Self {
        Self {
            kind: ErrorKind::DuplicateFieldSpec(name.into()),
            spec: Some(SpecKind::Field),
            source: None,
        }
    }

    pub fn field(mut self) -> Self {
        self.spec = Some(SpecKind::Field);
        self
    }

    pub fn hgram(mut self) -> Self {
        self.spec = Some(SpecKind::Hgram);
        self
    }

    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.source.as_ref()
            .map(|s| &**s as &(dyn std::error::Error + 'static))
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.spec {
            Some(s) => write!(fmt, "{}: {}", s, self.kind),
            None    => write!(fmt, "{}", self.kind),
        }
    }
}

impl std::convert::From<KeyLookupError> for Error {
    fn from(source: KeyLookupError) -> Self {
        Self {
            kind: ErrorKind::InvalidSpecFormat,
            spec: None,
            source: Some(Box::new(source) as Box<dyn std::error::Error + Sync + Send>),
        }
    }
}

// -------------------------------------------------------------------------------------------------
// ErrorKind

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum ErrorKind {

    /// Occurs when config is missing key that is marked as mandatory in specification
    RequiredKeyAbsent(String),

    /// Occurs when config contains field referencing capture name that is not configured (ie was
    /// not present in any Regex).
    RequiredCaptureAbsent(String),

    /// Occurs when config contains hgram referencing field name that is not configured nor
    /// automaticaly generated from capture name.
    RequiredFieldAbsent(String),

    /// Occurs when config contains two fields with same name but different specification
    ///
    /// We care only for `field` duplications since there is no way to distinguish them when
    /// creating histogram config from duplicate `field`. Multiple histogram configs using same
    /// field name is no issue since we may construct multiple different histograms over same
    /// field.
    DuplicateFieldSpec(String),

    /// Occurs when config specification format is invalid and can't be parsed properly
    InvalidSpecFormat,
}

impl std::fmt::Display for ErrorKind {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::RequiredKeyAbsent(key) =>
                write!(fmt, "missing required key '{}'", key),

            Self::RequiredCaptureAbsent(name) =>
                write!(fmt, "missing required regex capture '{}'", name),

            Self::RequiredFieldAbsent(name) =>
                write!(fmt, "missing required field '{}'", name),

            Self::DuplicateFieldSpec(name) =>
                write!(fmt, "duplicate specification for name '{}'", name),

            Self::InvalidSpecFormat =>
                fmt.write_str("specification format error"),
        }
    }
}

// -------------------------------------------------------------------------------------------------
// SpecKind

#[derive(Debug,Copy,Clone,PartialEq,Eq)]
pub enum SpecKind {
    Field, Hgram,
}

impl std::fmt::Display for SpecKind {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Field => fmt.write_str("field"),
            Self::Hgram => fmt.write_str("histogram"),
        }
    }
}

// -------------------------------------------------------------------------------------------------
// ErrorCause

#[derive(Debug)]
pub struct ErrorCause(pub Box<dyn std::error::Error + Send + Sync>);

impl<T> std::convert::From<T> for ErrorCause
where 
    T: std::error::Error + Send + Sync + 'static
{
    fn from(cause: T) -> Self {
        Self(Box::new(cause))
    }
}
