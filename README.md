# histosaur

Simple tool to extract data from text file(s) and aggregate occurences into
pretty histograms

## Usage

The `histosaur` tool takes one or more regular expressions passed via `--regex`
flag in the input to detemine what to look for in input file(s). Each *named*
capture group will create one histogram from content it captures.

## Examples

*Create histogram of number of lines per hour in `/var/log/syslog` file:*

```
$ histosaur -r '^(?P<name>...\s*\d\d? \d\d)' /var/log/syslog
```

We are not really counting lines, we are counting number of times we see
date-time fragment like `"Dec 12 17:20"` in the input.

Since the regular expression will match exactly one time per line (provided date
format matches), we will effectively compute number of lines per given day/time.


## License

Licensed under either of

* Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE)
  or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT)
  or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
