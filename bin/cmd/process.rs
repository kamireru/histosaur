use regex::Regex;
use histosaur::parse::Source;
use histosaur::parse::Matcher;
use histosaur::config::Outline;

// -------------------------------------------------------------------------------------------------
// Process

#[derive(Debug)]
pub struct Process {
    sources: Vec<Source>,
    regexes: Vec<Regex>,
    outline: Outline,
}

impl Process {

    pub fn new() -> Self {
        Self {
            sources: Vec::new(),
            regexes: Vec::new(),
            outline: Outline::new(),
        }
    }

    pub fn source(&mut self, source: Source) -> &Self {
        self.sources.push(source);
        self
    }

    #[allow(dead_code)]
    pub fn sources(&mut self, sources: impl IntoIterator<Item=Source>) -> &Self {
        self.sources.extend(sources);
        self
    }

    pub fn regex(&mut self, regex: Regex) -> &Self {
        self.regexes.push(regex);
        self
    }

    #[allow(dead_code)]
    pub fn regexes(&mut self, regexes: impl IntoIterator<Item=Regex>) -> &Self {
        self.regexes.extend(regexes);
        self
    }

    pub fn outline(&mut self, outline: impl Into<Outline>) -> &Self {
        self.outline = outline.into();
        self
    }
}

impl crate::cmd::Execute for Process {

    fn execute(self) -> anyhow::Result<()> {
        let mut hgramap = self.outline.build()?;
        let mut matcher = Matcher::new(&self.regexes);

        for source in self.sources {
            matcher.process_source_into(source, &mut hgramap)?;
        }

        for (_, hgram) in hgramap.into_iter() {
            println!("{}", hgram.output());
        }

        Ok(())
    }
}
