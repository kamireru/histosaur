// -------------------------------------------------------------------------------------------------
// Help

#[derive(Debug)]
pub struct Help(String);

impl Help {

    pub fn new(opts: &getopts::Options) -> Self {
        let usage = format!(
            indoc::indoc!{r###"
                Usage: {binary} [OPTIONS] --regex REGEX FILE...

                    Each named capture group in the regular expression is a
                    source of data for one histogram. The histogram name is
                    equal to the name of capture group.

                Examples:

                    Create histogram of log message count per day and hour in
                    the syslog logfile:
                    $ {binary} --regex '^(?P<name>...\s*\d\d? \d\d)' /var/log/syslog
            "###},
            binary = env!("CARGO_PKG_NAME")
        );
        Self(opts.usage(usage.as_str()))
    }
}

impl crate::cmd::Execute for Help {

    fn execute(self) -> anyhow::Result<()> {
        println!("{}", self.0);
        Ok(())
    }
}
