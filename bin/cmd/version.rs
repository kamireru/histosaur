// -------------------------------------------------------------------------------------------------
// Version

#[derive(Debug)]
pub struct Version(String);

impl Version {
    
    pub fn new() -> Self {
        Self(format!("{} {} ({})",
            env!("CARGO_PKG_NAME"),
            env!("VCS_COMMIT_NAME"),
            env!("VCS_COMMIT_DATE"),
        ))
    }
}

impl crate::cmd::Execute for Version {

    fn execute(self) -> anyhow::Result<()> {
        println!("{}", self.0);
        Ok(())
    }
}
