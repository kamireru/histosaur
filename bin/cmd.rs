mod process;
mod version;
mod help;

pub use process::*;
pub use version::*;
pub use help::*;

// -------------------------------------------------------------------------------------------------
// Cmd

#[derive(Debug)]
pub enum Cmd {
    Process(Process),
    Help(Help),
    Version(Version),
}

impl Execute for Cmd {
    fn execute(self) -> anyhow::Result<()> {
        match self {
            Self::Process(c) => c.execute(),
            Self::Version(c) => c.execute(),
            Self::Help(c)    => c.execute(),
        }
    }
}

impl std::convert::From<Process> for Cmd {
    fn from(process: Process) -> Self {
        Self::Process(process)
    }
}

impl std::convert::From<Version> for Cmd {
    fn from(version: Version) -> Self {
        Self::Version(version)
    }
}

impl std::convert::From<Help> for Cmd {
    fn from(help: Help) -> Self {
        Self::Help(help)
    }
}

// -------------------------------------------------------------------------------------------------
// Execute

pub trait Execute {
    fn execute(self) -> anyhow::Result<()>;
}
