mod cli;
mod cmd;

use cmd::Execute;

fn main() -> anyhow::Result<()> {
    cli::parse_argv()?
        .execute()
}
