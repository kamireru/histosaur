use std::str::FromStr;

use histosaur::config;
use histosaur::parse::Source;

use crate::cmd::Cmd;
use crate::cmd::Process;
use crate::cmd::Version;
use crate::cmd::Help;

pub fn parse_argv() -> anyhow::Result<Cmd> {
    let args: Vec<String> = std::env::args().skip(1).collect();
    Parser::new().parse(&args)
}

#[allow(dead_code)]
pub fn parse(args: &[String]) -> anyhow::Result<Cmd> {
    Parser::new().parse(args)
}

// -------------------------------------------------------------------------------------------------
// Parser

pub struct Parser(getopts::Options);

impl Parser {

    /// create new [`Options`] instance
    pub fn new() -> Self {
        let mut opts = getopts::Options::new();

        opts.optmulti("r", "regex", "regular expression to use for data extraction", "REGEX");
        opts.optmulti("f", "field", "field specification string", "SPEC");
        opts.optmulti("g", "histogram", "histogram specification string", "SPEC");

        opts.optflag("V", "version", "binary version info");
        opts.optflag("h", "help",  "binary help info");

        Self(opts)
    }

    pub fn parse(&self, args: &[String]) -> anyhow::Result<Cmd> {
        let matches = self.0.parse(args)?;

        if matches.opt_present("help") {
            return Ok(Help::new(&self.0).into())
        }
        if matches.opt_present("version") {
            return Ok(Version::new().into())
        }

        self.parse_process(&matches).map(Into::into)
    }

    fn parse_process(&self, matches: &getopts::Matches) -> anyhow::Result<Process> {
        use anyhow::Context;

        let mut process = Process::new();
        let mut outline = config::Outline::new();

        for spec in matches.opt_strs("regex").iter() {
            let regex = regex::Regex::new(spec.as_ref())?;

            outline.captures(regex.capture_names().filter_map(std::convert::identity));
            process.regex(regex);
        }

        for spec in matches.opt_strs("field") {
            let field = config::field::Outline::from_str(spec.as_ref())
                .with_context(|| format!("failed to parse config '{}'", spec))?;
            outline.field(field);
        }

        for spec in matches.opt_strs("histogram") {
            let hgram = config::hgram::Outline::from_str(spec.as_ref())
                .with_context(|| format!("failed to parse config '{}'", spec))?;
            outline.hgram(hgram);
        }

        for spec in &matches.free {
            process.source(Source::from(spec.as_ref()));
        }

        process.outline(outline);

        Ok(process)
    }
}
