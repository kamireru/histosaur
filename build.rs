// TODO: Add error handling for revision retrieval and fail build process
fn main() {
    let output = std::process::Command::new("git")
        .args(&["describe", "--tags", "--always", "HEAD"])
        .output()
        .unwrap();
    println!("cargo:rustc-env=VCS_COMMIT_NAME={}", String::from_utf8(output.stdout).unwrap());

    let output = std::process::Command::new("git")
        .args(&["log", "-1", "--format=%cd", "--date=iso", "HEAD"])
        .output()
        .unwrap();
    println!("cargo:rustc-env=VCS_COMMIT_DATE={}", String::from_utf8(output.stdout).unwrap());

}
